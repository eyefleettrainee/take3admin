import { Component } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  station = [
    {
      title: 'Central Phuket Parking 3',
      type: 'outdoor',
      detail: 'ที่จอดรถ zone C floor G near door 3',
      imgstation: './assets/img/48EF82B4-47BA-45DE-A01D-9EADFD55B49E-768x514.jpeg'
    },
    {
      title: 'Central Bangna Parking 2',
      type: 'indoor',
      detail: 'ที่จอดรถ zone B floor 3 near door 3',
      imgstation: './assets/img/48EF82B4-47BA-45DE-A01D-9EADFD55B49E-768x514.jpeg'
    },
    {
      title: 'Central World Parking 1',
      type: 'indoor',
      detail: 'ที่จอดรถ zone A floor 2 near door 1',
      imgstation: './assets/img/48EF82B4-47BA-45DE-A01D-9EADFD55B49E-768x514.jpeg'
    },
    {
      title: 'Siam Paragon Parking 4',
      type: 'outdoor',
      detail: 'ที่จอดรถ zone D floor B near door 4',
      imgstation: './assets/img/48EF82B4-47BA-45DE-A01D-9EADFD55B49E-768x514.jpeg'
    },
    {
      title: 'Emporium Parking 2',
      type: 'indoor',
      detail: 'ที่จอดรถ zone C floor 4 near door 2',
      imgstation: './assets/img/48EF82B4-47BA-45DE-A01D-9EADFD55B49E-768x514.jpeg'
    }
  ];
  
   
  
  
  stdetail: any;
  pushdetail(){
    this.stdetail.push()
  }
}
