import { Component } from '@angular/core';

@Component({
  selector: 'app-usertable',
  templateUrl: './usertable.component.html',
  styleUrls: ['./usertable.component.scss']
})
export class UsertableComponent {
  customers=[
    { id: 1, firstName: 'Pranee', lastName: 'Kanchana', email: 'Pranee@gmail.com', phoneNumber: '0585635354' },
    { id: 2, firstName: 'Sukhon', lastName: 'Wattana', email: 'Sukhon@gmail.com', phoneNumber: '0869342563' },
    { id: 3, firstName: 'Somchai', lastName: 'Tongsri', email: 'Somchai@gmail.com', phoneNumber: '0912345678' },
  ];
  deleteCustomer(customers: number) {
    // ดำเนินการลบลูกค้าที่มี customerId ที่กำหนด
    // เช่น เรียกใช้งาน API หรือดำเนินการอื่น ๆ
  }
}


