import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './component/dashboard/card/card.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { StatisticsComponent } from './component/dashboard/statistics/statistics.component';
import { UsertableComponent } from './component/usertable/usertable.component';


@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    DashboardComponent,
    StatisticsComponent,
    UsertableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
